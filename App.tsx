import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { AuthContext } from './src/app/views/contexts/AuthContext';
import { authReducer } from './src/app/views/reducers/authReducer';
import { ContentDrawer } from './src/app/views/components/contentDrawer/ContentDrawer';

import { Text, TouchableOpacity } from 'react-native'

import {
    SplashScreen,
    LoginScreen,
    InicialScreen,
    MenuScreen,
    SignUpScreen,
    ResetPassword,
    ObligacionesVehScreen,
    PagarObligacionesScreen,
    PqrScreen,
    MisVehiculosScreen,
    ResultadoPqrScreen,
    ListadoPqrScreen,
    FichaTecnicaScreen,
    ViewVehiculo,
    ViewScreen,
    Ajustes,
    Notificacion
    } from './src/app/views/include';



const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function App() {
  
  const [user, dispatchUser] = React.useReducer(authReducer, { logged:false });

  return (
    <AuthContext.Provider value={{user, dispatchUser}}>
    <NavigationContainer>

      {!user.logged? (
        <>
         <Stack.Navigator initialRouteName="Splash" headerMode="none">
           <Stack.Screen name="Splash" component={SplashScreen} />
           <Stack.Screen name="SignIn" component={LoginScreen} />
           <Stack.Screen name="SignUp" component={SignUpScreen} />
           <Stack.Screen name="ResetPassword" component={ResetPassword} />
          </Stack.Navigator>
        </>
     ) : (
        <> 
         <Drawer.Navigator initialRouteName="Inicio" 
         drawerContent={(prop)=><ContentDrawer {...prop}/>}
         drawerContentOptions={{
             activeTintColor: 'orange',
             //inactiveBackgroundColor:'white',
            // inactiveTintColor:'white',
             itemStyle: { marginVertical: 2 },

            }}>

           
           <Drawer.Screen options={{drawerLabel:'Menu'}}  name="Menu" component={MenuScreen} />  
           <Drawer.Screen options={{drawerLabel:'Notificaciones'}} name="Notificacion" component={Notificacion} /> 
           <Drawer.Screen options={{drawerLabel:'Ajustes'}} name="Ajustes" component={Ajustes} /> 
           <Drawer.Screen options={{drawerLabel:''}} name="Inicio" component={InicialScreen} />
           <Drawer.Screen options={{drawerLabel:''}} name="Pqr" component={PqrScreen} />      
           <Drawer.Screen options={{drawerLabel:''}} name="ResultadoPqr" component={ResultadoPqrScreen} />  
           <Drawer.Screen options={{drawerLabel:''}} name="ListadoPqr" component={ListadoPqrScreen} />  
           <Drawer.Screen options={{drawerLabel:''}} name="ObligacionesVeh"  component={ObligacionesVehScreen} />
           <Drawer.Screen options={{drawerLabel:''}} name="PagarObligacion" component={PagarObligacionesScreen} /> 
           <Drawer.Screen options={{drawerLabel:''}} name="MisVehiculos" component={MisVehiculosScreen} /> 
           <Drawer.Screen options={{drawerLabel:''}} name="FichaTecnica" component={FichaTecnicaScreen} />
           <Drawer.Screen options={{drawerLabel:''}} name="ViewVehiculo" component={ViewVehiculo} /> 
           <Drawer.Screen options={{drawerLabel:''}} name="ViewScreen" component={ViewScreen} /> 
           
       
        </Drawer.Navigator>
       </>
     )}
 
    </NavigationContainer>
    </AuthContext.Provider>
  );
}