export interface Credenciales {
    usuario: string,
    contrasenia: string,
}

export interface EndPoint {
    nombre: string,
    ruta: string,
    credenciales: Credenciales,
    otros: any
}