import { EndPoint } from "./endpoints";

export const environment: EndPoint[] = [
	{
		nombre: "auth",
		credenciales: {
			usuario: "",
			contrasenia: "",
		},
		otros: {
			platform: "SIGETT",
			project: ["TransArj"],
		},
		ruta: "https://isysauth.herokuapp.com/api/authdb",
	},
	{
		nombre: "sigett",
		credenciales: {
			usuario: "",
			contrasenia: "",
		},
		otros: {},
		ruta: "https://sigett.herokuapp.com/sigett.apiservice",
	},
];

export enum EndPointsId {
	auth = 0,
	sigett = 1,
}

export default environment;