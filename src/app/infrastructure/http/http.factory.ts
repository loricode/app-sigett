import { AuthService } from './auth.service';
import { SigettService } from './sigett.service';
import { HttpService } from './http.service';

type HttpType = 'SiggetService' | 'AuthService';

export class HttpFactory {
	public static service(httpType: HttpType): HttpService {
		if (httpType === 'AuthService') {
			return AuthService.auth;
        }else if(httpType === 'SiggetService'){
			return SigettService.service
		}else {
			return AuthService.auth;
		}
	}
}