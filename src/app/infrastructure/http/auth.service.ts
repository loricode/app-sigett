import * as globalEnv from '../../../environments/environment';
import { HttpService } from './http.service';

export class AuthService extends HttpService {
	private static _instance: AuthService;

	private constructor() {
		super(globalEnv.environment[globalEnv.EndPointsId.auth].ruta);
	}

	public static get auth(): HttpService {
		if (!AuthService._instance) {
			AuthService._instance = new AuthService();
		}
		return AuthService._instance;
	}
}