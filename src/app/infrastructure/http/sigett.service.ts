import { HttpService } from './http.service';
import * as globalEnv from '../../../environments/environment';

export class SigettService extends HttpService {
	private static _instance: SigettService;

	private constructor() {
		super(globalEnv.environment[globalEnv.EndPointsId.sigett].ruta);
	}

	public static get service(): HttpService {
		if (!SigettService._instance) {
			SigettService._instance = new SigettService();
		}
		return SigettService._instance;
	}
}