import { HttpFactory } from '../../infrastructure/http/http.factory';
import { objectDataListMap } from '../../domain/mapping/objectDataList.map';
import { Response } from '../../domain/interfaces/reponse/Response';
import * as response from '../../domain/interfaces/reponse';
import { ValueText } from '../componentsFormaters/ReactSelectInterface';

export class ConfigRepository {
	private _http = HttpFactory.service('SiggetService');
	private static _instance: ConfigRepository;

	private constructor() { }

	public static get service(): ConfigRepository {
		if (!ConfigRepository._instance) {
			ConfigRepository._instance = new ConfigRepository();
		}

		return ConfigRepository._instance;
	}


public async getTemaricasPQR(dataList?: boolean) {
    return await this.getConfiguracionPorEntidad('TEMATICAS_PQR', dataList);
}


public async getPropietarios(criterio: string) {
    let query = {};

    if (criterio.includes('@')) query = { correo: criterio };
    else if (!Number.isNaN(Number.parseInt(criterio))) query = { id: criterio };
    else query = { razon: criterio };

    return (
        await this._http.get<Response<response.RespTercero>>(
            '/propietarios/listar',
            { params: query })
    ).data.data;
}

public async getConfiguracionPorEntidad(entidad: string, dataList: boolean = false): Promise<response.RespConfiguracion[] | ValueText[]> {
    const configuraciones = (await this._http.get<Response<response.RespConfiguracion>>('/entidadconfig/listar', {
        params: { entidad }
    })).data.data

    if (dataList) {
        const configuraciones_ = objectDataListMap(configuraciones, 'id_unico', ['detalle']);
        return configuraciones_;
    } else {
        const formated: response.RespConfiguracion[] = configuraciones.map((config) => ({ ...config, entidad_config: entidad }));
        return formated;
    }
}


}