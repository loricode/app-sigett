import { Response } from '../../domain/interfaces/reponse/Response';
import * as response from '../../domain/interfaces/reponse/RespPropietarioDump';
import { HttpFactory } from '../../infrastructure/http/http.factory';

export class PropietarioRepository {
	private _http = HttpFactory.service('SiggetService');
	private static _instance: PropietarioRepository;

	private constructor() {}

	public static get service(): PropietarioRepository {
		if (!PropietarioRepository._instance) {
			PropietarioRepository._instance = new PropietarioRepository();
		}
		return PropietarioRepository._instance;
	}

	public async getPropietarioDump(idTercero: string) {
		const proptietario = (await this._http.get<Response<response.RespPropietarioDump>>(
			`/propietario/${ idTercero }/dump`
		)).data.data as any;

		return proptietario as response.RespPropietarioDump;
	}

}
