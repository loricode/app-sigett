import { HttpFactory } from "../http/http.factory";
import * as request from '../../domain/interfaces/request';


export class AuthTransaccion {
    private _http = HttpFactory.service('AuthService');
    private static _instance: AuthTransaccion;

    private constructor() {}

    public static get service(): AuthTransaccion {
        if (!AuthTransaccion._instance) {
            AuthTransaccion._instance = new AuthTransaccion();
        }
        return AuthTransaccion._instance;
    }

    public async login(data: request.User):Promise<any>  {
        return (await this._http.post<any>('/cuenta/verify', data)).data
    }

    public async verificarCuenta(correo: string): Promise<any>  {
        return (await this._http.get<any>(`/cuentas/list/correo/${correo}`)).data
    }
}