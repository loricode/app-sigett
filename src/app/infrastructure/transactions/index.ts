import { AuthTransaccion } from './auth.transaction';
import { OperadoresTransaccion } from './operadores.transaction';
import { PropietarioTransaction } from './propietario.transaction';
export {
    AuthTransaccion,
    OperadoresTransaccion,
    PropietarioTransaction
}