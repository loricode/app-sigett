import * as request from "../../domain/interfaces/request";
import { HttpFactory } from "../http/http.factory";

export class PropietarioTransaction {
    private _http = HttpFactory.service('SiggetService');
    private static _instance: PropietarioTransaction;

    private constructor() {}

    public static get service(): PropietarioTransaction {
        if (!PropietarioTransaction._instance) {
            PropietarioTransaction._instance = new PropietarioTransaction();
        }
        return PropietarioTransaction._instance;
    }

    public async crearPQR(data: request.ReqPQR): Promise<any> {
        const response = await this._http.post<any>(`/propietario/pqr/registrar`, data);
        //return response.data.data? true : false;
        return response.data;
    }  
}