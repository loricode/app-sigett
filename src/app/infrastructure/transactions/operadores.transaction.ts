import { HttpFactory } from "../http/http.factory";
import * as request from '../../domain/interfaces/request';


export class OperadoresTransaccion {
    private _http = HttpFactory.service('SiggetService');
    private static _instance: OperadoresTransaccion;

    private constructor() {}

    public static get service(): OperadoresTransaccion {
        if (!OperadoresTransaccion._instance) {
            OperadoresTransaccion._instance = new OperadoresTransaccion();
        }
        return OperadoresTransaccion._instance;
    }


    public async getOperadores(): Promise<any>  {
        return (await this._http.get<any>('/operadores/listar')).data
    }
}