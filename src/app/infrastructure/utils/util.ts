export function generarIdCaso() {
    const date = new Date();
    const suma = date.getHours() + date.getMinutes() + date.getSeconds();
    return `${ date.getFullYear() }-${suma}`;
  }