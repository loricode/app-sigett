export interface RespTercero {
    id_tercero: string;
    tipo_id: string;
    digverif: number;
    razon_social: string;
    apellido1: string;
    apellido2: string;
    nombre1: string;
    nombre2: string;
    telefono: number;
    contacto: string;
    email: string;
    direccion: string;
    direccion_c: string;
    cod_depto:number;
    cod_munic:number;
    id: number;
}