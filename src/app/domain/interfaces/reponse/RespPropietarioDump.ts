export interface RespPropietarioDump {
    propietario: Propietario;
    vehiculos: DumpVehiculo[];
    obligaciones: RespObligacion[];
    historialPQR: RespHistorialPQR[];
}

interface Propietario {
    id_tercero: string;
    razon_social: string;
    digverif: number;
    direccion: string;
    direccion_c: string;
    email: string;
    telefono: number;
}

export interface DumpVehiculo {
    placaveh: string;
    cod_claserv: number;
    clase_servicio: string;
    cod_claveh: number;
    clase_vehiculo: string;
    nro_motor: string;
    nro_chasis: string;
    modelo_anno: number;
    activa: string;
    propietario_actual: string;
    razon_social: string;
    fecha_matricula: string;
    nro_lic_tto: string;
    consec_caja: number;
    ultimo_anno_pago: number;
}

export interface RespObligacion {
    placaveh: string;
    nro_factura: string;
    anno_compromiso: number;
    valor: number;
    saldo_actual: number;
}

export interface RespHistorialPQR {
    id_caso: string;
    id_tercero_tramita: string;
    fecha_tramite: string;
    ref_canal_radicacion: string;
    canal_radicacion: string;
    ref_tematica_solicitud: string;
    tematica_pqr: string;
    anotaciones: string;
    estado: string;
}