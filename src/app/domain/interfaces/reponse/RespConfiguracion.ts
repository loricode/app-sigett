export interface RespConfiguracion {
    entidad_config: string;
    id_unico: string;
    detalle: string;
}