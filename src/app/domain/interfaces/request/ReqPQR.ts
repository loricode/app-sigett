export interface ReqPQR {
    id_tercero: number;
    canal: string;
    tema: string;
    detalles: string;
    id_caso: string;
}