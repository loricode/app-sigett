import { ValueText } from '../../infrastructure/componentsFormaters/ReactSelectInterface';

export const objectDataListMap = (arr: any[], value_column: string | number, text_columns: Array<string | number>): ValueText[] => {
    return arr.map(item => {
        const objRetorno = text_columns.map((text_column) => {  
            let newValor = item[text_column] + ' ';

            return newValor
        })
        return {
            value: item[value_column],
            text: objRetorno.join('')
        }
    })
}