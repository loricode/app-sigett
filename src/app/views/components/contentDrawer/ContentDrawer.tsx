import * as React from 'react';

import { Avatar } from "react-native-paper";
import { Ionicons } from '@expo/vector-icons';
import { AuthContext } from '../../contexts/AuthContext';
import {
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
  } from '@react-navigation/drawer';
import { Text, View } from 'react-native';


export function ContentDrawer({...props}:any){
  const { user, dispatchUser }: any = React.useContext(AuthContext);

    return (
        <DrawerContentScrollView {...props}>
        
        <View style={{flex:0.4, paddingTop:6}}>
          <View style={{flexDirection:'row'}}>
             <View style={{flex:1}}>
             <Avatar.Icon icon="account-circle-outline"
               style={{backgroundColor:'black', marginLeft:12}} size={45} color="orange" />
    
             </View>    
            <View style={{flex:3, paddingTop:15}}>
               <Text style={{fontSize:10}}>{user.payload.nombre_asociado}</Text>
            </View>
               
          </View> 
         
        </View>

       <View style={{flex:0.2}}>
         <DrawerItem 
           onPress={() => props.navigation.navigate("Menu")}
           {...props}
           labelStyle={{ color: "#fbae41", fontSize: 16 }}
           label="Menu" icon={({size, color})=> (
           < Ionicons name="list" size={size} color={color}/>
          )}/> 
        </View>

        <View style={{flex:1}}>
         <DrawerItem 
           onPress={() => props.navigation.navigate("Notificacion")}
           {...props}
           labelStyle={{ color: "#fbae41", fontSize: 16 }}
           label="Notificaciones" icon={({size, color})=> (
            <Ionicons name="md-notifications-outline" size={size} color={color} />
          )}/> 
        </View>

        <View style={{flex:0.2}}>
         <DrawerItem 
           onPress={() => props.navigation.navigate("Ajustes")}
           {...props}
           labelStyle={{ color: "#fbae41", fontSize: 16 }}
           label="Ajustes" icon={({size, color})=> (
            <Ionicons name="md-construct-outline" size={size} color={color} />
          )}/> 
        </View>

        <View style={{flex:0.5}}>
         <DrawerItem 
           onPress={() => dispatchUser({type: 'logout'})}
           {...props}
           labelStyle={{ color: "#fbae41", fontSize: 16 }}
           label="Cerrar sesión" icon={({size, color})=> (
           < Ionicons name="md-exit-outline" size={size} color={color}/>
          )}/> 
        </View>
        {/* <DrawerItemList {...props} /> */}
       
      </DrawerContentScrollView>
    );
}