import React, { useRef } from 'react';
import { Animated } from 'react-native';

export const FadeInView = (props:any) => {
  const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0

  React.useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
          toValue:1,
          duration:1000,
          useNativeDriver:true
        } 
    ).start();
  }, [fadeAnim])

  return (
    <Animated.View                 
      style={{
        ...props.style,
        opacity: fadeAnim,         
      }}
    >
      {props.children}
    </Animated.View>
  );
}