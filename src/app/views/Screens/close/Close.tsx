import * as React from 'react';
import { View, Text} from 'react-native';
import { AuthContext } from '../../contexts/AuthContext'

export function Close({navigation}:any){
    const { user, dispatchUser }: any = React.useContext(AuthContext)

    React.useEffect(()=>{
        dispatchUser({type:'logout'})
    },[])
    return (
        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                    
        </View>
    );
}