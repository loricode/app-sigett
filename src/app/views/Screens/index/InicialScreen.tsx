import * as React from 'react';
import { StyleSheet, StatusBar,Pressable, TouchableOpacity, Text, KeyboardAvoidingView, TextInput, View } from 'react-native';
import { AuthContext } from '../../contexts/AuthContext';
import {Carousel} from '../../components/carousel/Carousel';
import { Ionicons } from '@expo/vector-icons';
import * as repositories from '../../../infrastructure/repositories';
import * as responseInterfaces from '../../../domain/interfaces/reponse';

export function InicialScreen({ navigation }: any) {
    const [checked, onChange] = React.useState(false);
    const { user, dispatchUser }: any = React.useContext(AuthContext);

    React.useEffect(() => {   
      (async function () {
        let newPayload: any = {};
        let terceroDump: responseInterfaces.RespPropietarioDump | null = null;
        const tercero = await repositories.ConfigRepository.service.getPropietarios(user.payload.correo);
  
        if (tercero.length !== 0) {
          terceroDump = await repositories.PropietarioRepository.service.getPropietarioDump(tercero[0].id_tercero);
        }
  
        newPayload = { 
          ...user.payload,
          ndid: tercero[0].id_tercero,
        }
  
        if (terceroDump) newPayload['dump'] = terceroDump;
  
        dispatchUser({ type: 'login', payload: newPayload });
      })();
    }, [])


    return (
      <View style={[styles.container, {flexDirection: "column"}]}>

        <View style={{ height:60, backgroundColor: "#F4BE22"  ,flexDirection: 'row'}}>
                
          <View style={{ flex: 5, paddingTop: 20 }} >
            <Text style={styles.titleNavbar}>Transito Arjona</Text>
          </View>
                
          <View style={{ flex: 1, paddingTop: 10 }} >
            <TouchableOpacity>
            </TouchableOpacity>
            </View>
        </View>

        <Text style={{textAlign:'center',fontSize:15, marginVertical:4, color:'#E19F06'}}>
            Novedades y Noticias
        </Text>

        <View style={{flex:4 ,marginVertical:4}}><Carousel/></View>

        <Text style={{textAlign:'center',fontSize:14, marginVertical:4}}>
          Oficinas Tramites y Horarios
        </Text>
       
       <View style={{flex:4}}><Carousel/></View>
      
      <View style={{flex:4, paddingTop:2}}> 
    
        <View style={styles.checkAndText}>
          <View style={{ flex: 1 }}>     
              <Pressable
                style={[styles.checkboxBase, checked && styles.checkboxChecked]}
                onPress={()=>onChange(!checked)}>
                {checked && <Ionicons name="checkmark" size={24} color="white" />}
                </Pressable>
              </View>
              <View style={{ flex: 5 }} >
                <Text style={{textAlign:'center'}}>
                   Acepto términos y condiciones de uso del servicio de información.
                 </Text>
               </View>
         </View>
       </View>

       <View style={{ height: 60, backgroundColor: "#E19F06", flexDirection:'row-reverse',alignItems:'center'}}>
         <TouchableOpacity
            disabled={!checked}
            style={{marginHorizontal:10}}
            onPress={() => navigation.navigate("Menu")}>
            <Text style={styles.textButton}>{checked? "Continuar >" : null}</Text>
          </TouchableOpacity>
       </View>
       </View>
    );
  }

const styles = StyleSheet.create({
  container: {
      flex: 1,
    
      paddingTop: StatusBar.currentHeight
  },
  buttonCerrarSesion: {
      backgroundColor: 'yellow',
      padding: 15,
      marginBottom: 10
  },
  textButton: {
      color: '#fff',
      fontSize: 17,
      textAlign:'center'
  },
  buttonContinuar: {
      backgroundColor: '#5183F7',
      padding: 15,
      width: '80%',
      marginBottom: 10,
      borderRadius: 5
  },
  header: {
     flex: 1,
     backgroundColor: '#F4BE22',
     paddingTop: StatusBar.currentHeight,
     flexDirection: 'row',
     alignItems:'center'
  },
  checkboxBase: {
     width: 24,
     height: 24,
     justifyContent: 'center',
     alignItems: 'center',
     borderRadius: 4,
     borderWidth: 2,
     borderColor: 'blue',
     backgroundColor: 'transparent',
     marginLeft:17
  },
  checkboxChecked: {
     backgroundColor: 'blue',
  },   
  buttonDisabled: {
    backgroundColor: '#EAEDF4',
    padding: 15,
    width: '80%',
    marginBottom: 10,
    borderRadius: 5
  },
  iconStyle:{
    marginTop: 2,
    textAlign: 'center'
  },
  titleNavbar:{
    fontSize:16,
    marginHorizontal:8,
    color:'white'
 },
 footer:{
   backgroundColor:'#E19F06',
   flex:1,
   alignItems:'center',
   flexDirection:'row-reverse',
 },
 checkAndText:{
   flex: 1,
   flexDirection: 'row',
   alignItems:'center',
 }
})