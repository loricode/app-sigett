import * as React from 'react';
import { StyleSheet, View, Text, StatusBar, TouchableOpacity } from 'react-native';
import { AuthContext } from '../../contexts/AuthContext';
import {Carousel} from '../../components/carousel/Carousel';
import { Ionicons } from '@expo/vector-icons';
export function MenuScreen({ navigation }: any) {

    const { user, dispatchUser }: any = React.useContext(AuthContext);
 
    return (
        <View style={[styles.container, {flexDirection: "column"}]}>

        <View style={{ height:60, backgroundColor: "#F4BE22", flexDirection: 'row'}}>
         <View style={{flex: 5, paddingTop: 20}}>
          <Text style={styles.titleNavbar}>Novedades y Noticias</Text>
         </View>

         <View style={{ flex: 1, paddingTop: 10 }} >
            <TouchableOpacity>
            </TouchableOpacity>
            </View>
        </View>
      
        <View style={{flex:2}}><Carousel/></View>
        <View style={{flex:4, paddingTop:3, justifyContent:'center'}}> 
       
        <View style={{paddingTop:3, alignItems:'center', justifyContent:'center'}}>
            <TouchableOpacity
                style={styles.buttonContinuar}
                onPress={() => navigation.navigate('Pqr')}>
                <Text style={styles.textButton}>Peticiones Quejas y Reclamos</Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={styles.buttonContinuar}
                onPress={() => navigation.navigate('MisVehiculos')}>
                <Text style={styles.textButton}>Mis Vehiculos</Text>
            </TouchableOpacity>
 
            <TouchableOpacity
                style={styles.buttonContinuar}
                onPress={() => navigation.navigate('ViewScreen')}>
                <Text style={styles.textButton}>Mis obligaciones de impuesto</Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={styles.buttonContinuar}
                onPress={() => navigation.navigate('ViewScreen')}>
                <Text style={styles.textButton}>Descargar factura de pago</Text>
            </TouchableOpacity>

         </View>
         </View> 

         <View style={{backgroundColor:'#EDEAE1', height: 60,  alignItems: 'center', justifyContent: 'center'}}>
           <Text style={{fontSize:15}}>{user.payload.nombre_asociado}</Text>
           <Text style={{textAlign:'center'}}> Empresa de Tránsito y Transporte de Bolivar © 2021 Powered By Sigett</Text>
         </View>

         </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight
    },
    buttonCerrarSesion: {
        backgroundColor: 'yellow',
        padding: 15,
        marginBottom: 10
    },
    textButton: {
        color: '#fff',
        fontSize: 17
    },
    buttonContinuar: {
        backgroundColor: '#F4BE22',
        padding: 15,
        width: '80%',
        marginBottom: 10,
        borderRadius: 5
    },
    titleNavbar:{
        marginLeft:12,
        fontSize:17,
        color:'#fff'
      }
      

})