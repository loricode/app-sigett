import * as React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, StatusBar,
    Image
} from 'react-native';

export function SplashScreen({ navigation }: any) {
    return (
        <View style={[styles.container, {flexDirection: "column"}]}>
             <View style={{ height:60, backgroundColor: "#F4BE22", flexDirection: 'row'}}>
                
             <View style={{flex: 5, paddingTop: 20}}>
          <Text style={styles.titleNavbar}>Transito Arjona</Text>
         </View>

         <View style={{ flex: 1, paddingTop: 10 }} >
            <TouchableOpacity></TouchableOpacity>
            </View>
            </View>

            <View style={{flex:4, alignItems:'center', justifyContent:'center'}}>

            <Image  style={styles.image}      
              source={ require('../../../../../assets/splash-transito-arjona.png')} />
            </View>

            <View  style={{backgroundColor:'#E19F06', height: 60,alignItems:'center',
        flexDirection:'row-reverse',}}>
                   <TouchableOpacity
                        style={{marginHorizontal:10}}
                        onPress={() => navigation.navigate("SignIn")}>
                       <Text style={styles.textButton}>{"Continuar >"}</Text>
                  </TouchableOpacity>
            </View>
          </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight
    },
  
    iconStyle:{
        marginTop: 2,
        textAlign: 'center'
    },
    textButton: {
        fontSize: 16,
        color: "#fff",
        textAlign: 'center'
    },
    titleNavbar:{
        fontSize:16,
        marginHorizontal:8,
        color:'white'
    },
    image: {
         width:"100%",
       //  resizeMode: "cover",
         height:"100%"   
      }
})