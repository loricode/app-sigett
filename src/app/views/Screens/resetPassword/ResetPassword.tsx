import * as React from 'react';
import { StyleSheet, Button, TouchableOpacity, Text, KeyboardAvoidingView, TextInput, View } from 'react-native';
import { AuthContext } from '../../contexts/AuthContext';

export function ResetPassword({ navigation }: any) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button onPress={() => navigation.goBack()} title="Go back home" />
      </View>
    );
  }