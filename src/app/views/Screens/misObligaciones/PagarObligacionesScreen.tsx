import * as React from 'react';
import {
   StyleSheet,
   View,
   Text,
   StatusBar,
   TouchableOpacity,
  } from 'react-native';
;
import { AuthContext } from '../../contexts/AuthContext';
import { Ionicons } from '@expo/vector-icons';
import Barcode from '../../components/barcode/Barcode';

export function PagarObligacionesScreen({ route, navigation }: any) {
  const { user }: any = React.useContext(AuthContext);
  let { factura, placa, annio } = route.params

    return (
      <View style={[styles.container, {flexDirection: "column"}]}>

      <View style={{ height:60, backgroundColor: "#F4BE22"  ,flexDirection: 'row'}}>
     
        <View style={{flex: 5, paddingTop: 20}}>
          <Text style={styles.titleNavbar}>
            Obligaciones de vehículos
          </Text>
       </View>
  
    <View style={{ flex: 1 , paddingTop: 10 }} >
      <TouchableOpacity 
        onPress={() => navigation.openDrawer()}>
        <Ionicons
          name="md-list"
          size={35}
          style={{ textAlign: 'center' }}
          color="white"
       />
     </TouchableOpacity>
   </View>
  </View>    

     <View style={{flex:4, paddingTop:5}}>
     <Text style={{fontSize:18, fontWeight:'bold', color:'#000', textAlign:'center'}}>
            Notificacion de cobro enviada
          </Text>

     <View style={{
          flex:0.4,
          justifyContent:'center',
          marginVertical:5,
          backgroundColor: "white",
          marginHorizontal:6,
          borderRadius:6,
          shadowRadius: 1,
          shadowColor: "#000",
          elevation:5,
          alignItems:'center'
        }}>
           <Text style={{fontSize:22}}>{placa}</Text>
           <Text style={{fontSize:15}}>
             {`Año: ${annio}`}
           </Text>
           <Text style={{fontSize:15}}>
             {`${user.payload.correo}`}
           </Text>
          </View>

          <View style={{  
              alignItems:'center'}}>
             <Barcode
              value={factura}
              options={{ format: '', background: 'white' }}
              rotation={0}
             />
          </View>

    </View>

    <View style={{ height: 60, backgroundColor: "#E19F06", flexDirection:'row-reverse',alignItems:'center'}} >
       <TouchableOpacity
          style={{marginHorizontal:10}}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textButton}>{"Regresar >"}</Text>
        </TouchableOpacity>
      </View>
     </View>
    );
  }


const styles = StyleSheet.create({
    container:{
      flex: 1, 
      paddingTop: StatusBar.currentHeight
    },
    buttonCerrarSesion:{
      backgroundColor:'red',
      padding:15,
      marginBottom:10
    },
    textButton:{
      color:'#fff',
      fontSize:17
    },
    button: {
      backgroundColor: '#F4BE22',
      padding: 10,
      marginBottom: 6,
      width: '100%',
      borderRadius: 5
  },
 
    textArea: {
      width: '100%',
      padding: 8,
      borderRadius: 10,
      borderColor: '#4812DB',
      borderWidth: 1,
      marginBottom: 14,
      textAlign: 'center',
  },
    buttonContinuar: {
      backgroundColor: '#F4BE22',
      padding: 15,
      marginBottom: 10
    },
    header: {
      flex: 1,
      backgroundColor: '#F4BE22',
      paddingTop: StatusBar.currentHeight,
      flexDirection: 'row',
      alignItems:'center'
  },
    footer:{
      backgroundColor:'#E19F06',
      flex:1,
      alignItems:'center',
      flexDirection:'row-reverse',
    },
    iconStyle:{
      marginTop: 2,
      textAlign: 'center'
  },
  item: {
     backgroundColor: "white",
     padding: 20,
     marginVertical: 8,
     shadowColor: "#000",
     elevation: 5,
     shadowRadius: 4,
     borderRadius:4,
     width:'95%',
     marginLeft:8
  },
  title: {
    fontSize: 20,
    textAlign:'center'
  },
  subTitle: {
    fontSize: 15,
    textAlign:'center'
  },
  textBodyItem:{
    fontSize:13,
    textAlign:'center',
    color:'#fff'
  },
  titleNavbar:{
    marginLeft:12,
    fontSize:17,
    color:'#fff'
  }
});