import * as React from 'react';
import {
   StyleSheet,
   View,
   Text,
   StatusBar,
   TouchableOpacity,
   ScrollView,
  } from 'react-native';

import * as responseInterfaces from '../../../domain/interfaces/reponse';
import { AuthContext } from '../../contexts/AuthContext';
import { Ionicons } from '@expo/vector-icons';


export function ObligacionesVehScreen({route, navigation }: any) {
  const { user }: any = React.useContext(AuthContext);
  let {placaveh, modelo_anno, clase_vehiculo} =  route.params
 
  const obligaciones: responseInterfaces.RespObligacion[] = user.payload.dump.obligaciones.filter(
    (obligacion: any) => obligacion.placaveh === placaveh
  );

  function currencyFormat(num:number) {
    return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
 }

 function calcularTotal(obligaciones: responseInterfaces.RespObligacion[]) {
  let total = 0;
  obligaciones.forEach((obligacion) => total += obligacion.valor);

  return total;
}

    return (
      <View style={[styles.container, {flexDirection: "column"}]}>
     
     <View style={{ height:60, backgroundColor: "#F4BE22"  ,flexDirection: 'row'}}>
     
        <View style={{flex: 5,paddingTop: 20}}>
          <Text style={styles.titleNavbar}>
            Obligaciones de vehículos
          </Text>
       </View>
  
    <View style={{ flex: 1, paddingTop: 10 }} >
      <TouchableOpacity 
        onPress={() => navigation.openDrawer()}>
        <Ionicons
          name="md-list"
          size={35}
          style={{ textAlign: 'center' }}
          color="white"
       />
     </TouchableOpacity>
   </View>
  </View>    

     <View style={{flex:1, paddingTop:2}}>
     <View style={{
          flex:0.4,
          justifyContent:'center',
          marginVertical:4,
          backgroundColor: "white",
          marginHorizontal:6,
          borderRadius:6,
          shadowRadius: 1,
          shadowColor: "#000",
          elevation:5
        }}>
           <Text style={{marginLeft:4, fontSize:22}}>{placaveh}</Text>
           <Text style={{marginLeft:4, fontSize:17}}>
             {`${clase_vehiculo} | Modelo: ${modelo_anno}`}
           </Text>
          </View>

          <View style={{flex:1, marginHorizontal:6, shadowColor: "#000"}}>
          <Text style={{
            backgroundColor:"#000",
            color:"#fff",
            padding:8}}>
            Facturas
           </Text>
           
            <ScrollView>
            {obligaciones.map((item, i) =>(
               <View key={i} style={{flexDirection:'row', marginBottom:1}}>
             
               <View style={{
                flex:2,
                padding:15,
                backgroundColor:'#E19F06',
                borderTopLeftRadius:2,
                alignItems:'center'
               }}>
               <Text style={{
                 color:'#000',
                 justifyContent:'center',
                 fontSize:16,
                 fontWeight:'bold'
                 }}>{item.anno_compromiso}</Text>
             </View>
 
             <View style={{
                flex:2,
                padding:15,
                backgroundColor:'#fff',
                borderTopRightRadius:2,
                alignItems:'center'}}>
    
              <Text style={{
                color:'#000',
                justifyContent:'center',
                fontSize:16,
                fontWeight:'bold'}}>{currencyFormat(item.saldo_actual)}</Text>
             </View>

             <View style={{
                flex:1,
                padding:15,
                backgroundColor:'#fff',
                borderTopRightRadius:2,
                alignItems:'center'}}>
    
              <TouchableOpacity  onPress={() => navigation.navigate("PagarObligacion", {
                "factura":item.nro_factura,
                "placa":item.placaveh,
                "annio":item.anno_compromiso
               })}>
              <Ionicons name="md-barcode" size={26} color="black" />
              </TouchableOpacity>
             </View>
              </View>
             ) ) }
            </ScrollView>
           
       </View>
    </View>

    <View style={{height: 60, marginBottom:1, backgroundColor:'#F4BE22', flexDirection:'row-reverse',alignItems:'center'}}>
        <TouchableOpacity
          style={{marginHorizontal:10}}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textButton}> Total: { currencyFormat(calcularTotal(obligaciones))}</Text>
        </TouchableOpacity>
    </View>

    <View style={{ height: 60, backgroundColor: "#E19F06", flexDirection:'row-reverse',alignItems:'center'}} >
       <TouchableOpacity
          style={{marginHorizontal:10}}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textButton}>{"Regresar >"}</Text>
        </TouchableOpacity>
      </View>
    </View>
    );
  }

const styles = StyleSheet.create({
    container:{
      flex: 1, 
      paddingTop: StatusBar.currentHeight
    },
    buttonCerrarSesion:{
      backgroundColor:'red',
      padding:15,
      marginBottom:10
    },
    textButton:{
      color:'#fff',
      fontSize:17
    },
    button: {
      backgroundColor: '#F4BE22',
      padding: 10,
      marginBottom: 6,
      width: '100%',
      borderRadius: 5
  },
 
    textArea: {
      width: '100%',
      padding: 8,
      borderRadius: 10,
      borderColor: '#4812DB',
      borderWidth: 1,
      marginBottom: 14,
      textAlign: 'center',
  },
    buttonContinuar: {
      backgroundColor: '#F4BE22',
      padding: 15,
      marginBottom: 10
    },
    header: {
      flex: 1,
      backgroundColor: '#F4BE22',
      paddingTop: StatusBar.currentHeight,
      flexDirection: 'row',
      alignItems:'center'
  },
    footer:{
      backgroundColor:'#E19F06',
      flex:1,
      alignItems:'center',
      flexDirection:'row-reverse',
    },
    iconStyle:{
      marginTop: 2,
      textAlign: 'center'
  },
  item: {
    backgroundColor: "white",
    padding: 20,
    marginVertical: 8,
    shadowColor: "#000",
    elevation: 5,
    shadowRadius: 4,
    borderRadius:4,
    width:'95%',
    marginLeft:8
  },
  title: {
    fontSize: 20,
    textAlign:'center'
  },
  subTitle: {
    fontSize: 15,
    textAlign:'center'
  },

  textBodyItem:{
    fontSize:13,
    textAlign:'center',
    color:'#fff'
  },
  titleNavbar:{
    marginLeft:12,
    fontSize:17,
    color:'#fff'
  }
});
