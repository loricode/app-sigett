import * as React from 'react';
import { StyleSheet, View, Text, StatusBar, TouchableOpacity} from 'react-native';
import { AuthContext } from '../../contexts/AuthContext';
import { Ionicons } from '@expo/vector-icons';
export function ViewScreen(){
    const { user, dispatchUser }: any = React.useContext(AuthContext);

    return (
     <View style={{flex:1, paddingTop: StatusBar.currentHeight, flexDirection: "column"}}>
        <View style={{ height:60, backgroundColor: "#F4BE22", flexDirection: 'row'}}>
         <View style={{flex: 5, paddingTop: 20}}>
          <Text style={styles.titleNavbar}>Mis obligaciones de impuesto</Text>
         </View>

         <View style={{ flex: 1, paddingTop: 10 }} >
            <TouchableOpacity
              onPress={() => dispatchUser({ type: 'logout'})}>
                <Ionicons name="md-exit-outline" size={35} color="white" />
              </TouchableOpacity>
            </View>
        </View>
      
         <Text>screen en construcción</Text>
     </View>
    ); 
}

const styles = StyleSheet.create({
    titleNavbar:{
        marginLeft:12,
        fontSize:17,
        color:'#fff'
      }
})