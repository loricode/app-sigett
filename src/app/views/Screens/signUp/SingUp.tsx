import * as React from 'react';
import {
   StyleSheet,
   StatusBar,
   SafeAreaView,
   ScrollView,
   TouchableOpacity,
   Text,
   TextInput,
   View } from 'react-native';



export function SignUpScreen({ navigation }: any) {

  const [date, setDate] = React.useState(new Date())

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>

      <View style={{ flexDirection: 'row', alignSelf:'center' }}>
          
          <TextInput 
             placeholder="Nombre"
          
          //onChangeText={(text: string) => setInput({ ...input, correo: text })}
            style={styles.input}
          //value={input.correo}
          />
        </View>


        <View style={{ flexDirection: 'row', alignSelf:'center' }}>
          <TextInput placeholder="Identificacion"
          //onChangeText={(text: string) => setInput({ ...input, correo: text })}
            style={styles.input}
          //value={input.correo}
          />
        </View>


        <View style={{ flexDirection: 'row', alignSelf:'center' }}>
          
          <TextInput 
             placeholder="Fecha de nacimieno"
          
          //onChangeText={(text: string) => setInput({ ...input, correo: text })}
            style={styles.input}
          //value={input.correo}
          />
        </View>

        <View style={{ flexDirection: 'row', alignSelf:'center' }}>
          
          <TextInput 
             placeholder="correo"
          
          //onChangeText={(text: string) => setInput({ ...input, correo: text })}
            style={styles.input}
          //value={input.correo}
          />
        </View>

        <View style={{ flexDirection: 'row', alignSelf:'center' }}>
          
          <TextInput 
             placeholder="dirección"
          
          //onChangeText={(text: string) => setInput({ ...input, correo: text })}
            style={styles.input}
          //value={input.correo}
          />
        </View>

        <View style={{ flexDirection: 'row', alignSelf:'center' }}>
          
          <TextInput 
             placeholder="Ciudad"
          
          //onChangeText={(text: string) => setInput({ ...input, correo: text })}
            style={styles.input}
          //value={input.correo}
          />
        </View>
        <View style={{ flexDirection: 'row', alignSelf:'center' }}>
          
          <TextInput 
             placeholder="Télefono"
          
          //onChangeText={(text: string) => setInput({ ...input, correo: text })}
            style={styles.input}
          //value={input.correo}
          />
        </View>
     
      
       
        
        <View style={{ flexDirection: 'row', alignSelf:'center'}}>
          <TouchableOpacity style={styles.button}>
            <Text style={{color:'white', textAlign:'center'}}>Guardar</Text>
          </TouchableOpacity>
        </View>

        {/* <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Button onPress={() => navigation.goBack()} title="Go back home" />
        </View> */}
      </ScrollView>
    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    
  },
  scrollView: {
    marginHorizontal: 20,
    paddingTop:10
  },
  text: {
    fontSize: 42,
  },
  input: {
    width: '80%',
    padding: 8,
    borderRadius: 10,
    borderColor: '#000',
    borderWidth: 1,
    marginBottom: 9,
    textAlign: 'center'
},
button: {
  backgroundColor: '#4812DB',
  padding: 10,
  marginBottom: 6,
  width: '80%',
  borderRadius: 5
},
});
