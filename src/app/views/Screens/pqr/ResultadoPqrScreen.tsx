import * as React from 'react';
import { StyleSheet, View, Text, Button, StatusBar, TouchableOpacity } from 'react-native';


export function ResultadoPqrScreen({ navigation }:any){
    return (
        <View style={styles.container}>
        <Text style={{fontSize:25}}>
          MovileResultadoPqrScreen
        </Text>
        <Button onPress={() => navigation.goBack()} title="Regresar" />

      
      </View>
    );
}

const styles = StyleSheet.create({
    container:{
      flex: 1, 
      alignItems: 'center', 
      justifyContent: 'center',
      paddingTop: StatusBar.currentHeight
    },
    buttonCerrarSesion:{
      backgroundColor:'red',
      padding:15,
      marginBottom:10
    },
    textButton:{
      color:'#fff',
      fontSize:17
    },
    buttonContinuar:{
      backgroundColor:'blue',
      padding:15,
    }
  
  });