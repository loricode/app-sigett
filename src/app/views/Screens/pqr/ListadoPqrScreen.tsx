import * as React from 'react';
import {
   StyleSheet,
   View,
   Text,
   StatusBar,
   TouchableOpacity,
   VirtualizedList,
   ActivityIndicator  
  } from 'react-native';
import * as repositories from '../../../infrastructure/repositories';
import * as responseInterfaces from '../../../domain/interfaces/reponse';
import { AuthContext } from '../../contexts/AuthContext';
import { Ionicons } from '@expo/vector-icons';

interface State {
  repositoryData: responseInterfaces.RespHistorialPQR[],
  loading:boolean
}

const getItem = (data:responseInterfaces.RespHistorialPQR[], index:number) => ({  
    id:index,
    id_caso:data[index].id_caso,
    tematica_pqr:data[index].tematica_pqr,
    estado:data[index].estado
});


export function ListadoPqrScreen({navigation}:any){
  const { user }: any = React.useContext(AuthContext);
  
  const [state, setState] = React.useState<State>({
    repositoryData: [],
    loading:false
  });

  React.useEffect(() => {
    (async function () {
      setState(prevState => ({
         repositoryData:prevState.repositoryData,   
         loading:false
      }));
      const responseTematicas = await repositories.PropietarioRepository.service.getPropietarioDump(user.payload.ndid)
      setState({
        repositoryData:responseTematicas.historialPQR,
        loading:true
      })

    })();
  }, [user])

  const Item = ({ data }:any) => {
     const opcionesEstado = {
       R: 'Radicado',
       P: 'Pendiente',
       A: 'Atendida'
     } as any;
   
     return (
     <View style={styles.item}>
       <Text style={styles.title}>{`${data.id_caso}`}</Text>
       <Text style={styles.textBodyItem}>{`<< ${data.tematica_pqr} >>`}</Text>
       <View style={{flexDirection:'row'}}>
        <View style={{flex:2, backgroundColor:'#E19F06', borderTopLeftRadius:2, alignItems:'center'}}>
         <TouchableOpacity style={{padding:2}}>
         <Text>Estado: {opcionesEstado[data.estado || '']}</Text>
         </TouchableOpacity>
         </View>
         <View style={{flex:2, padding:2, backgroundColor:'black', borderTopRightRadius:2, alignItems:'center'}}>
         <TouchableOpacity >
            <Text style={{color:'#fff'}}>Anotaciones</Text>
         </TouchableOpacity>
         </View>
       </View>
   
     </View>
   );
 }

  return ( 
    <View style={[styles.container, {flexDirection: "column"}]}>

      <View style={{ height:60, backgroundColor: "#F4BE22"  ,flexDirection: 'row'}}>

       <View style={{ flex: 5 , paddingTop: 20 }} >
         <Text style={styles.titleNavbar}>Peticiones, Quejas y reclamos</Text>
       </View>
      
      <View style={{ flex: 1 , paddingTop: 10}} >
        <TouchableOpacity
          onPress={() => navigation.openDrawer()}>
          <Ionicons
           name="md-list"
           size={35}
           style={styles.iconStyle}
           color="white"
          />
        </TouchableOpacity>
      </View>
    </View>

    <View style={{ flex: 4, alignItems:'center', justifyContent:'center'}} >
    { state.repositoryData.length === 0 && state.loading  && (<Text>No hay registros</Text>) }
      { state.loading ? 
          <VirtualizedList
          initialNumToRender={6}
          data={state.repositoryData}
          renderItem={({ item }) => <Item data={item} />}
          keyExtractor={(item:any) => item.id_caso}
          getItem={getItem}
          getItemCount={() => state.repositoryData.length}
        />
      : <ActivityIndicator size="large" color="#0000ff"/> } 

 
    </View>
    
    <View style={{ height: 60, backgroundColor: "#E19F06", flexDirection:'row-reverse',alignItems:'center'}} >
       <TouchableOpacity
          style={{marginHorizontal:10}}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textButton}>{"Regresar >"}</Text>
        </TouchableOpacity>
      </View>
    </View>
 );
}

const styles = StyleSheet.create({
    container:{
      flex: 1, 
      paddingTop: StatusBar.currentHeight
    },
    buttonCerrarSesion:{
      backgroundColor:'red',
      padding:15,
      marginBottom:10
    },
    textButton:{
      color:'#fff',
      fontSize:17
    },
    button: {
      backgroundColor: '#F4BE22',
      padding: 10,
      marginBottom: 6,
      width: '100%',
      borderRadius: 5
  },
 
    textArea: {
      width: '100%',
      padding: 8,
      borderRadius: 10,
      borderColor: '#4812DB',
      borderWidth: 1,
      marginBottom: 14,
      textAlign: 'center',
  },
    buttonContinuar: {
      backgroundColor: '#F4BE22',
      padding: 15,
      marginBottom: 10
    },
    header: {
      flex: 1,
      backgroundColor: '#F4BE22',
      paddingTop: StatusBar.currentHeight,
      flexDirection: 'row',
      alignItems:'center'
  },
    footer:{
      backgroundColor:'#E19F06',
      flex:1,
      alignItems:'center',
      flexDirection:'row-reverse',
    },
    iconStyle:{
      marginTop: 2,
      textAlign: 'center'
  },
  item: {
    backgroundColor: "white",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    shadowColor: "#000",
    elevation: 5,
    shadowRadius: 4,
    borderRadius:4
  },
  title: {
    fontSize: 20,
    textAlign:'center'
  },

  textBodyItem:{
    fontSize:15,
    textAlign:'center'
  },
  titleNavbar:{
    marginLeft:12,
    fontSize:17,
    color:'#fff'
  }
});