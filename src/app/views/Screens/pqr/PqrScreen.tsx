import * as React from 'react';
import { 
  StyleSheet,
  View,
  Text,
  StatusBar,
  Alert,
 TouchableOpacity,
 ActivityIndicator,
 TextInput } from 'react-native';
import { RadioButton } from 'react-native-paper';
import { AuthContext } from '../../contexts/AuthContext';
import { Ionicons } from '@expo/vector-icons';
import * as repositories from '../../../infrastructure/repositories';
import * as responseInterfaces from '../../../domain/interfaces/reponse';
import * as transactions from '../../../infrastructure/transactions';
import * as utils  from '../../../infrastructure/utils/util';

interface State {
  repositoryData: responseInterfaces.RespConfiguracion[];
  anotaciones: string,
  loading:boolean
}

export function PqrScreen({ navigation }: any) {

  const { user }: any = React.useContext(AuthContext);
  const [selectedTematica, setSelectedTematica] = React.useState("Seleccione");
  const [{checked}, setChecked ] =React.useState({ checked: '' })
  const [state, setState] = React.useState<State>({
    repositoryData: [],  
    anotaciones: '',
    loading:false
  });

  React.useEffect(() => {
    (async function () {
      if(state.repositoryData.length === 0){
        setState(prevState =>({ ...prevState, loading:false }))
        const responseTematicas = await repositories.ConfigRepository.service.getTemaricasPQR() as responseInterfaces.RespConfiguracion[];
        setState(prevState =>({ ...prevState, repositoryData: responseTematicas, loading:true }))
      }
   
    })();
  }, [])


  async function saveButtonHandle() {

      const isSaved = await transactions.PropietarioTransaction.service.crearPQR({
      id_tercero:user.payload.dump.propietario.id_tercero,
      canal: 'APP',
      detalles: state.anotaciones,
      tema: selectedTematica,
      id_caso: utils.generarIdCaso()
    });
  
    if (isSaved.message ==="ok") {
      Alert.alert(
        "PQR",
        "!Registro exitoso¡",
        [
          { text: "OK", onPress: () => null }
        ]
      );
    } else {
      Alert.alert(
        "PQR",
        isSaved.message,
        [
          { text: "OK", onPress: () => null }
        ]
      );
    }
  }


  return (
    <View style={[styles.container, {flexDirection: "column"}]}>
      <View style={{ height:60, backgroundColor: "#F4BE22"  ,flexDirection: 'row'}}>

          <View style={{ flex: 5 , paddingTop: 20 }} >
             <Text style={styles.titleNavbar}>Peticiones quejas y reclamos</Text>
           </View>
                
            <View style={{ flex: 1 , paddingTop: 10}} >
              <TouchableOpacity
                onPress={() => navigation.navigate("ListadoPqr")}>
                <Ionicons
                  name="md-list"
                  size={35}
                  style={styles.iconStyle}
                  color="white"
                />
              </TouchableOpacity>
              </View>
              </View>

      <View style={{ flex: 4, alignItems:'center', justifyContent:'center'}} >
       
      { state.loading ? 
        <View style={{alignItems:'center', marginBottom:5}}>

        {state.repositoryData.map((item:responseInterfaces.RespConfiguracion, i:number) => (
         <View key={i} style={{flexDirection: 'row', marginHorizontal:40}}>
        
         <View style={{ flex: 0.6 }} >
         <RadioButton
           color="#E19F06"
           value={item.id_unico}
           status={checked === `${item.id_unico}` ? 'checked' : 'unchecked'}
           onPress={() => { setChecked({ checked: item.id_unico}); }}
         />
         </View>
           
         <View style={{ flex: 5 , paddingTop: 7 }} >
            <Text style={{fontSize:15, color:'black'}}>{item.detalle} </Text>
         </View> 
 
          </View> 
           ))}
       
        </View>
       : <ActivityIndicator size="large" color="#0000ff"/> }

      <TextInput 
         style={styles.textArea}
         placeholder="Anotacion de solicitud"
         multiline={true}
         onChangeText={(text)=> setState(prevState =>
            ({ ...prevState, anotaciones:text  }))}
         numberOfLines={5}
       />

      <View style={{alignItems:'center'}}>
           <TouchableOpacity
             style={styles.button}
             onPress={saveButtonHandle}>
             <Text style={styles.textButton}>
                Continuar
              </Text>        
          </TouchableOpacity>
        </View>
     
      </View>
      
      <View style={{ height: 60, backgroundColor: "#E19F06", flexDirection:'row-reverse',alignItems:'center'}} >
       <TouchableOpacity
          style={{marginHorizontal:10}}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textButton}>{"Regresar >"}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight
  },
  button: {
    backgroundColor: '#F4BE22',
    padding: 10,
    marginBottom: 6,
    width: 264,
    borderRadius: 5
},
  textButton: {
    color: '#fff',
    fontSize: 17,
    textAlign:'center',
    width: '100%'
  },
  textArea: {
    width: 264,
    padding: 8,
    borderRadius: 10,
    borderColor: '#F4BE22',
    borderWidth: 1,
    marginBottom: 14,
    textAlign: 'center',
},
 
  iconStyle:{
    marginTop: 2,
    textAlign: 'center'
},
titleNavbar:{
  marginLeft:12,
  fontSize:17,
  color:'#fff'
},
checkboxBase: {
  width: 24,
  height: 24,
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: 4,
  borderWidth: 2,
  borderColor: 'blue',
  backgroundColor: 'transparent',
  marginLeft:17
},
checkboxChecked: {
  backgroundColor: 'blue',
},
checkAndText:{
  flex: 1,
  flexDirection: 'row',
  alignItems:'center',
}

});