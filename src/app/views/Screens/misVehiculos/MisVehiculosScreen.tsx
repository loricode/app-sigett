import * as React from 'react';
import {
   StyleSheet,
   View,
   Text,
   StatusBar,
   TouchableOpacity,
   FlatList,
   ActivityIndicator } from 'react-native';
import * as repositories from '../../../infrastructure/repositories';
import * as responseInterfaces from '../../../domain/interfaces/reponse';
import { AuthContext } from '../../contexts/AuthContext';
import { Ionicons } from '@expo/vector-icons';



const Item = ({ item, navigation }:any) => {

  return (
  <View style={styles.item}>
    <Text style={styles.title}>{`${item.placaveh}`}</Text>
    <Text style={styles.subTitle}>{`Modelo: ${item.modelo_anno}`}</Text>
    <View style={{flex:1, flexDirection:'row'}}>
    <View style={{flex:2, padding:2, backgroundColor:'black', borderTopRightRadius:2, alignItems:'center'}}>
     
    <Text 
      onPress={()=>navigation.navigate("FichaTecnica", {
        item
       })}
    style={styles.textBodyItem}>{`${item.clase_vehiculo}`}</Text>
    </View>

      <View style={{flex:2, padding:2, backgroundColor:'black', borderTopRightRadius:2, alignItems:'flex-end'}}>    
      <TouchableOpacity
        onPress={()=>navigation.navigate("FichaTecnica", {
          item,
       })} >
         <Text  style={{color:'#fff', fontSize:16}}> {'>'} </Text>
      </TouchableOpacity>
      </View>
    </View>

  </View>
);
}

interface State {
  repositoryData: responseInterfaces.RespHistorialPQR[],
  loading:boolean
}
    
export function MisVehiculosScreen({navigation}:any){
  const { user }: any = React.useContext(AuthContext);
  
  const [state, setState] = React.useState<State>({
    repositoryData: [],
    loading:true
  });

  const renderItem = ({ item }:any) => (
    <Item 
        item={item}
        navigation={navigation}
    />
  );

  return ( 
    <View style={[styles.container, {flexDirection: "column"}]}>

       <View style={{ height:60, backgroundColor: "#F4BE22"  ,flexDirection: 'row'}}>
         <View style={{flex: 5, paddingTop: 20}}>
           <Text style={styles.titleNavbar}>
             Mis Vehiculos
           </Text>
        </View>

     <View style={{ flex: 1, paddingTop: 10 }} >
       <TouchableOpacity 
         onPress={() => navigation.openDrawer()}>
         <Ionicons
           name="md-list"
           size={35}
           style={{ textAlign: 'center' }}
           color="white"
        />
      </TouchableOpacity>
    </View>  
    </View>


    <View style={{ flex: 4, justifyContent:'center'}} >
      { state.loading ? 
          <FlatList
           data={user.payload.dump.vehiculos || []}
           renderItem={renderItem}
           keyExtractor={(item, i) => item.placaveh +"-"+i}
        />
      : <ActivityIndicator size="large" color="#0000ff"/> } 
    </View>

    <View style={{ height: 60, backgroundColor: "#E19F06", flexDirection:'row-reverse',alignItems:'center'}} >
       <TouchableOpacity
          style={{marginHorizontal:10}}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textButton}>{"Regresar >"}</Text>
        </TouchableOpacity>
      </View>
  </View>
 );
}

const styles = StyleSheet.create({
    container:{
      flex: 1, 
      paddingTop: StatusBar.currentHeight
    },
    buttonCerrarSesion:{
      backgroundColor:'red',
      padding:15,
      marginBottom:10
    },
    textButton:{
      color:'#fff',
      fontSize:17
    },
    button: {
      backgroundColor: '#F4BE22',
      padding: 10,
      marginBottom: 6,
      width: '100%',
      borderRadius: 5
  },
 
    textArea: {
      width: '100%',
      padding: 8,
      borderRadius: 10,
      borderColor: '#4812DB',
      borderWidth: 1,
      marginBottom: 14,
      textAlign: 'center',
  },
    buttonContinuar: {
      backgroundColor: '#F4BE22',
      padding: 15,
      marginBottom: 10
    },
    header: {
      flex: 1,
      backgroundColor: '#F4BE22',
      paddingTop: StatusBar.currentHeight,
      flexDirection: 'row',
      alignItems:'center'
  },
    // footer: {
    //   backgroundColor: '#EDEAE1',
    //   flex: 1,
    //   alignItems: 'center',
    //   justifyContent: 'center'
    // },
    footer:{
      backgroundColor:'#E19F06',
      flex:1,
      alignItems:'center',
      flexDirection:'row-reverse',
    },
    iconStyle:{
      marginTop: 2,
      textAlign: 'center'
  },
  item: {
    backgroundColor: "white",
    padding: 20,
    marginVertical: 8,
    shadowColor: "#000",
    elevation: 5,
    shadowRadius: 4,
    borderRadius:4,
    width:'95%',
    marginLeft:8
  },
  title: {
    fontSize: 20,
    textAlign:'center'
  },
  subTitle: {
    fontSize: 15,
    textAlign:'center'
  },

  textBodyItem:{
    fontSize:13,
    textAlign:'center',
    color:'#fff'
  },
  titleNavbar:{
    marginLeft:12,
    fontSize:17,
    color:'#fff'
  }
  
  
  });