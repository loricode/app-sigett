import * as React from 'react';
import {
   StyleSheet,
   View,
   Text,
   StatusBar,
   TouchableOpacity,
   FlatList,
   ActivityIndicator } from 'react-native';
import * as repositories from '../../../infrastructure/repositories';
import * as responseInterfaces from '../../../domain/interfaces/reponse';
import { AuthContext } from '../../contexts/AuthContext';
import { Ionicons } from '@expo/vector-icons';


export function ViewVehiculo({ route, navigation }: any) {
    let {item} = route.params
   
    return (
      <>
      <View style={styles.header}>
     
        <View style={{flex: 5}}>
          <Text style={styles.titleNavbar}>
            Mis Vehiculos
          </Text>
       </View>
  
    <View style={{ flex: 1 }} >
      <TouchableOpacity 
        onPress={() => navigation.openDrawer()}>
        <Ionicons
          name="md-list"
          size={35}
          style={{ textAlign: 'center' }}
          color="white"
       />
     </TouchableOpacity>
   </View>
  </View>    

     <View style={styles.container}>
   
        
    </View>

     <View style={styles.footer}>
        <TouchableOpacity
          style={{marginHorizontal:10}}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textButton}>{"Regresar >"}</Text>
        </TouchableOpacity>
    </View>

        {/* <Button onPress={() => navigation.goBack()} title="Go back home" />
         <Text>{itemId}</Text> 
      </View> */}
      </>
    );
  }


const styles = StyleSheet.create({
    container:{
      flex: 9, 
      paddingTop: StatusBar.currentHeight
    },
    buttonCerrarSesion:{
      backgroundColor:'red',
      padding:15,
      marginBottom:10
    },
    textButton:{
      color:'#fff',
      fontSize:17
    },
    button: {
      backgroundColor: '#F4BE22',
      padding: 10,
      marginBottom: 6,
      width: '100%',
      borderRadius: 5
  },
 
    textArea: {
      width: '100%',
      padding: 8,
      borderRadius: 10,
      borderColor: '#4812DB',
      borderWidth: 1,
      marginBottom: 14,
      textAlign: 'center',
  },
    buttonContinuar: {
      backgroundColor: '#F4BE22',
      padding: 15,
      marginBottom: 10
    },
    header: {
      flex: 1,
      backgroundColor: '#F4BE22',
      paddingTop: StatusBar.currentHeight,
      flexDirection: 'row',
      alignItems:'center'
  },
    // footer: {
    //   backgroundColor: '#EDEAE1',
    //   flex: 1,
    //   alignItems: 'center',
    //   justifyContent: 'center'
    // },
    footer:{
      backgroundColor:'#E19F06',
      flex:1,
      alignItems:'center',
      flexDirection:'row-reverse',
    },
    iconStyle:{
      marginTop: 2,
      textAlign: 'center'
  },
  item: {
    backgroundColor: "white",
    padding: 20,
    marginVertical: 8,
    shadowColor: "#000",
    elevation: 5,
    shadowRadius: 4,
    borderRadius:4,
    width:'95%',
    marginLeft:8
  },
  title: {
    fontSize: 20,
    textAlign:'center'
  },
  subTitle: {
    fontSize: 15,
    textAlign:'center'
  },

  textBodyItem:{
    fontSize:13,
    textAlign:'center',
    color:'#fff'
  },
  titleNavbar:{
    marginLeft:12,
    fontSize:17,
    color:'#fff'
  }
  
  
  });