import * as React from 'react';
import {
   StyleSheet,
   View,
   Text,
   StatusBar,
   TouchableOpacity,
   FlatList,
   ActivityIndicator } from 'react-native';
import * as repositories from '../../../infrastructure/repositories';
import * as responseInterfaces from '../../../domain/interfaces/reponse';
import { AuthContext } from '../../contexts/AuthContext';
import { Ionicons } from '@expo/vector-icons';


export function FichaTecnicaScreen({ route, navigation }: any) {
    let { item } = route.params

    return (
      <View style={[styles.container, {flexDirection: "column"}]}>
    
     
       <View style={{ height:60, backgroundColor: "#F4BE22"  ,flexDirection: 'row'}}>
       <View style={{flex: 5, paddingTop: 20}}>
           <Text style={styles.titleNavbar}>
             Ficha técnica
           </Text>
       </View>
  
    <View style={{ flex: 1, paddingTop: 13  }} >
      <TouchableOpacity 
        onPress={() => navigation.navigate("ObligacionesVeh", {
          "placaveh":item?.placaveh,
          "modelo_anno":item?.modelo_anno,
          "clase_vehiculo":item?.clase_vehiculo
        })}>
          <Ionicons name="md-cash-outline" size={35} color="white" />
     </TouchableOpacity>
   </View>
  </View>    

     <View style={{flex:4}}>
         <View style={{
          flex:0.4,
          justifyContent:'center',
          marginVertical:5,
          backgroundColor: "white",
          marginHorizontal:6,
          borderRadius:6,
          shadowRadius: 1,
          shadowColor: "#000",
          elevation:5
        }}>
           <Text style={{marginLeft:4, fontSize:22}}>{item?.placaveh}</Text>
           <Text style={{marginLeft:4, fontSize:17}}>
             {`${item.clase_vehiculo} | Modelo: ${item?.modelo_anno}`}
           </Text>
          </View>

       <View style={{flex:0.8, marginHorizontal:6, shadowColor: "#000"}}>
          <Text style={{
            backgroundColor:"#000",
            color:"#fff",
            padding:8}}>
            Anotaciones
           </Text>
           
          <View style={{flexDirection:'row'}}>
             <View style={{
               flex:2,
               padding:15,
               backgroundColor:'#E19F06',
               borderTopLeftRadius:2,
               alignItems:'center'
              }}>
              <Text style={{
                color:'#000',
                justifyContent:'center',
                fontSize:16,
                fontWeight:'bold'
                }}>Servicio</Text>
            </View>

            <View style={{
               flex:2,
               padding:15,
               backgroundColor:'#fff',
               borderTopRightRadius:2,
               alignItems:'center'}}>
   
             <Text style={{
               color:'#000',
               justifyContent:'center',
               fontSize:16,
               fontWeight:'bold'}}>{item.clase_servicio}</Text>
            </View>
       </View>

    <View style={{flexDirection:'row', marginVertical:2}}>
     <View style={{
        flex:2,
        padding:15,
        backgroundColor:'#E19F06',
        borderTopLeftRadius:2,
        alignItems:'center'}}>
       <Text style={{
          color:'#000',
          justifyContent:'center',
          fontSize:16,
          fontWeight:'bold'}}>Motor</Text>
      </View>

      <View style={{
          flex:2,
          padding:15,
          backgroundColor:'#fff',
          borderTopRightRadius:2,
          alignItems:'center'}}>
   
         <Text style={{
           color:'#000',
           justifyContent:'center',
           fontSize:16,
           fontWeight:'bold'}}>{item?.nro_motor}</Text>
     
      </View>
    </View>

    <View style={{flexDirection:'row'}}>

     <View style={{
        flex:2,
        padding:15,
        backgroundColor:'#E19F06',
        borderTopLeftRadius:2,
        alignItems:'center'}}>
      <Text style={{
         color:'#000',
         justifyContent:'center',
         fontSize:16, 
         fontWeight:'bold'}}>Chasis</Text>
      </View>

      <View style={{
          flex:2,
          padding:15,
          backgroundColor:'#fff',
          borderTopRightRadius:2,
          alignItems:'center'}}>
   
         <Text style={{
            color:'#000',
            justifyContent:'center',
            fontSize:14,
            fontWeight:'bold'}}>{item.nro_chasis}</Text>
      </View>
    </View>
 </View>  

       <View style={{
            borderWidth:1,
            flex:0.6,
            marginVertical:5,
            backgroundColor:'#E19F06',
            marginHorizontal:6
          }}>
              <View style={{flexDirection:'row', marginTop:14,}}>
     <View style={{flex:2, padding:12, borderTopLeftRadius:2}}>
      <Text style={{
           color:'#000',
           fontSize:16,
           fontWeight:'bold'}}>Fecha de matricula</Text>
      </View>

      <View style={{flex:2, padding:12, borderTopRightRadius:2, alignItems:'center'}}>
   
         <Text style={{
            color:'#000',
            justifyContent:'center',
            fontSize:16,
            fontWeight:'bold'}}>{item?.fecha_matricula}</Text>
      </View>
    </View>

    <View style={{flexDirection:'row', marginVertical:2}}>
     <View style={{
        flex:2,
        padding:12,
        borderTopLeftRadius:2,
        alignItems:'center'}}>
      <Text style={{
         color:'#000',
         justifyContent:'center',
         fontSize:16,
         fontWeight:'bold'}}>Licencia de Tránsito</Text>
      </View>

      <View style={{
          flex:2,
          padding:12,
          borderTopRightRadius:2,
          alignItems:'center'}}>
   
         <Text style={{
             color:'#000',
             justifyContent:'center',
             fontSize:16,
             fontWeight:'bold'}}>{item?.nro_lic_tto}</Text>
      </View>
    </View>

    <View style={{flexDirection:'row'}}>
     <View style={{
        flex:2,
        padding:15,
        borderTopLeftRadius:2,
        alignItems:'center'}}>

      <Text style={{
        color:'#000',
        justifyContent:'center',
        fontSize:16,
        fontWeight:'bold'}}>Pago de Impuestos</Text>
      </View>

      <View style={{flex:2, padding:15, borderTopRightRadius:2, alignItems:'center'}}>
         <Text style={{
            color:'#000',
            justifyContent:'center',
            fontSize:1,
            fontWeight:'bold'}}>{item?.ultimo_anno_pago}</Text>
       </View>
    </View>
   </View>       
  </View>

  <View style={{ height: 60, backgroundColor: "#E19F06", flexDirection:'row-reverse',alignItems:'center'}} >
       <TouchableOpacity
          style={{marginHorizontal:10}}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textButton}>{"Regresar >"}</Text>
        </TouchableOpacity>
      </View>
    </View>
    );
  }


const styles = StyleSheet.create({
    container:{
      flex: 1, 
      paddingTop: StatusBar.currentHeight
    },
    buttonCerrarSesion:{
      backgroundColor:'red',
      padding:15,
      marginBottom:10
    },
    textButton:{
      color:'#fff',
      fontSize:17
    },
    button: {
      backgroundColor: '#F4BE22',
      padding: 10,
      marginBottom: 6,
      width: '100%',
      borderRadius: 5
  },
 
    textArea: {
      width: '100%',
      padding: 8,
      borderRadius: 10,
      borderColor: '#4812DB',
      borderWidth: 1,
      marginBottom: 14,
      textAlign: 'center',
  },
    buttonContinuar: {
      backgroundColor: '#F4BE22',
      padding: 15,
      marginBottom: 10
    },
    header: {
      flex: 1,
      backgroundColor: '#F4BE22',
      paddingTop: StatusBar.currentHeight,
      flexDirection: 'row',
      alignItems:'center'
  },
    footer:{
      backgroundColor:'#E19F06',
      flex:1,
      alignItems:'center',
      flexDirection:'row-reverse',
    },
    iconStyle:{
      marginTop: 2,
      textAlign: 'center'
  },
  item: {
    backgroundColor: "white",
    padding: 20,
    marginVertical: 8,
    shadowColor: "#000",
    elevation: 5,
    shadowRadius: 4,
    borderRadius:4,
    width:'95%',
    marginLeft:8
  },
  title: {
    fontSize: 20,
    textAlign:'center'
  },
  subTitle: {
    fontSize: 15,
    textAlign:'center'
  },

  textBodyItem:{
    fontSize:13,
    textAlign:'center',
    color:'#fff'
  },
  titleNavbar:{
    marginLeft:12,
    fontSize:17,
    color:'#fff'
  }
  });