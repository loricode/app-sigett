import * as React from 'react';
import { StyleSheet, TouchableOpacity, Text, KeyboardAvoidingView, TextInput, View } from 'react-native';
import { AuthContext } from '../../contexts/AuthContext';

import { platform, project } from '../../../../environments/globalEnv';
import { AuthTransaccion, OperadoresTransaccion } from '../../../infrastructure/transactions';
import { FadeInView } from '../../components/fadeInView/FadeInView';

export function LoginScreen({ navigation }: any) {
    const { dispatchUser }: any = React.useContext(AuthContext);
    const [input, setInput] = React.useState({ correo: '', clave: '' })
    const [mesagge, setMessage] = React.useState('');
    const [userData, setUserData] = React.useState<any>({});
    
    async function handlerVerifyEmail() {
        const response = await AuthTransaccion.service.verificarCuenta(input.correo)
        response.ok
            ? checkPlatformAndProject(response.data.autorizaciones, response)
            : setMessage(response.message);
    }


    const checkPlatformAndProject = (autorizaciones: any, response: any) => {
        const userData = autorizaciones.find(
            (autorizacion: any) =>
                autorizacion.proyecto === project &&
                autorizacion.estado === 'A' &&
                autorizacion.plataforma === platform
        );
        userData !== undefined
            ? setUserData(response)
            : setMessage('Cuenta sin permisos');
    };


    const handlerLogin = async () => {
        setMessage('');
        const response = await AuthTransaccion.service.login(input)
        response.ok ? getOperator(response.data) : setMessage(response.message);
      };
    

      const getOperator = async (payload: any) => {
        const { data } = await OperadoresTransaccion.service.getOperadores()
        const [currentOperator]: any = checkOperator(data);
        const newPayload: any = { ...payload, ...currentOperator };
        dispatchUser({ type: 'login', payload: newPayload });
      };

      const checkOperator = (operators: any[] = []) => { 
        return operators.filter((operator) => operator.email === input.correo);
      };
    

 return (
    <KeyboardAvoidingView style={styles.container}> 
          <FadeInView>
            <View style={styles.card}>
                <Text style={styles.textAutenticate}>Autenticación</Text>
                <Text>{mesagge}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <TextInput placeholder="correo"
                        autoFocus={true}
                        editable={(userData.data === undefined)}
                        onChangeText={(text: string) => setInput({ ...input, correo: text })}
                        style={styles.input}
                        value={input.correo}
                    />
                </View>
                {userData.data !== undefined && (
                   
                    <View style={{ flexDirection: 'row',width:'100%' }}>      
                        <TextInput placeholder="clave"
                            onChangeText={(text: string) => setInput({ ...input, clave: text })}
                            style={styles.input}
                            value={input.clave} 
                            textContentType="password"
                            />         
                    </View>     
                )}

                {userData.data === undefined ? (
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={handlerVerifyEmail}>
                            <Text style={styles.textButton}>
                                Continuar
                            </Text>
                        </TouchableOpacity>
                    </View>

                ) : (
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={handlerLogin}>
                            <Text style={styles.textButton}>
                                Iniciar Sesión
                        </Text>
                        </TouchableOpacity>
                    </View>
                )}
              {userData.data === undefined ? (
                <View style={{ backgroundColor: '#F2EFF8', padding: 10 }}>
                    <Text style={styles.textRegistrarse}
                        onPress={() => navigation.navigate('SignUp')}
                    >Registrarse</Text>
                </View>
                 ) : ( null )}
            </View>
          
        </FadeInView>
    </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        width: '80%',
        padding: 8,
        borderRadius: 10,
        borderColor: '#4812DB',
        borderWidth: 1,
        marginBottom: 9,
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#4812DB',
        padding: 10,
        marginBottom: 6,
        width: '80%',
        borderRadius: 5
    },
    card: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        borderRadius: 4,
        margin: 5,
        padding: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,

        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 3,
        
    },
    textAutenticate: {
        fontSize: 15,
        marginBottom: 5
    },
    textButton: {
        fontSize: 16,
        color: "#fff",
        textAlign: 'center'
    },
    textRegistrarse: {
        textAlign: 'center',
        color: '#4812DB',
        borderBottomColor: '#4812DB',
        borderBottomWidth: 1
    }
});
