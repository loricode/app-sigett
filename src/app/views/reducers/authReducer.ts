export const authReducer = (state = { logged: false }, action:any) => {
    switch (action.type) {
      case 'login':
        return { payload: action.payload, logged: true };
      case 'logout':
        return { user: {}, logged: false };
      default:
        return state;
    }
};