import { SplashScreen } from './Screens/splash/SplashScreen';
import { LoginScreen } from './Screens/loginScreen/LoginScreen';
import { InicialScreen } from './Screens/index/InicialScreen';
import { MenuScreen } from './Screens/index/MenuScreen';
import { SignUpScreen } from './Screens/signUp/SingUp';
import { ResetPassword } from './Screens/resetPassword/ResetPassword';
import { ObligacionesVehScreen } from './Screens/misObligaciones/ObligacionesVehScreen';
import { PagarObligacionesScreen } from './Screens/misObligaciones/PagarObligacionesScreen';
import { PqrScreen } from './Screens/pqr/PqrScreen';
import { MisVehiculosScreen } from './Screens/misVehiculos/MisVehiculosScreen';
import { FichaTecnicaScreen } from './Screens/misVehiculos/FichaTecnicaScreen';
import { ViewVehiculo } from './Screens/misVehiculos/ViewVehiculo';
import { ResultadoPqrScreen } from './Screens/pqr/ResultadoPqrScreen';
import { ListadoPqrScreen } from './Screens/pqr/ListadoPqrScreen';
import { ViewScreen } from './Screens/component404/ViewScreen';
import { Notificacion } from './Screens/Notificacion/Notificacion';
import { Ajustes } from './Screens/ajustes/Ajustes';
import {Close} from './Screens/close/Close';
export {
    Close,
    Notificacion,
    Ajustes,
    LoginScreen,
    InicialScreen,
    MenuScreen,
    SignUpScreen,
    ResetPassword,
    ObligacionesVehScreen,
    PagarObligacionesScreen,
    PqrScreen,
    ResultadoPqrScreen,
    MisVehiculosScreen,
    FichaTecnicaScreen,
    ListadoPqrScreen,
    SplashScreen,
    ViewVehiculo,
    ViewScreen
}